import argparse
import os

from application.manager import ReleaseManager


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--access_token", nargs='?', default=None, type=str, required=False)
    args = parser.parse_args()

    gitlab_access_token = args.access_token if args.access_token is not None else os.environ["CI_PRIVATE_TOKEN"]

    ReleaseManager(
        project_id=os.environ["CI_PROJECT_ID"],
        gitlab_access_token=gitlab_access_token
    ).run()
