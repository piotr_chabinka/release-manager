ARG VERSION=3.7.4
ARG DISTRO=stretch

FROM python:${VERSION}-${DISTRO} as builder

ARG PYTHONUSERBASE=/install
ENV PYTHONUSERBASE=$PYTHONUSERBASE
ENV PATH="$PYTHONUSERBASE/bin:$PATH"

RUN mkdir -p $PYTHONUSERBASE
WORKDIR $PYTHONUSERBASE

ARG TMP_DIR=/tmp/requirements
COPY ./requirements.txt ${TMP_DIR}/

RUN pip3 install --user --upgrade --ignore-installed pip
RUN pip3 install --user --ignore-installed --upgrade -r ${TMP_DIR}/requirements.txt

FROM python:${VERSION}-slim-${DISTRO}

RUN DEBIAN_FRONTEND=noninteractive apt-get update && apt-get install -y libmagic1 && apt-get clean && rm -rf /var/lib/apt/lists/*

COPY --from=builder /install /usr/local

ARG WORKDIR=/app
ENV WORKDIR=${WORKDIR}
WORKDIR ${WORKDIR}

COPY ./ ${WORKDIR}
