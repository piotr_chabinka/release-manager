from application.tag import Tag


def test_tags():

    assert Tag(major=1, minor=0, patch=0) == Tag(major=1, minor=0, patch=0)
    assert not Tag(major=1, minor=0, patch=0) == Tag(major=1, minor=0, patch=1)
    assert Tag(major=1, minor=0, patch=1) > Tag(major=1, minor=0, patch=0)
    assert Tag(major=1, minor=1, patch=0) > Tag(major=1, minor=0, patch=0)
    assert Tag(major=1, minor=1, patch=1) > Tag(major=1, minor=0, patch=0)
    assert Tag(major=1, minor=0, patch=0) < Tag(major=1, minor=0, patch=1)
    assert Tag(major=1, minor=0, patch=0) < Tag(major=1, minor=1, patch=0)
    assert Tag(major=1, minor=0, patch=0) < Tag(major=1, minor=1, patch=1)
    assert Tag(major=1, minor=0, patch=0) < Tag(major=1, minor=0, patch=1)
    assert Tag(major=1, minor=0, patch=0) <= Tag(major=1, minor=0, patch=1)
    assert Tag(major=1, minor=0, patch=0) <= Tag(major=1, minor=0, patch=0)
    assert Tag(major=1, minor=0, patch=1) >= Tag(major=1, minor=0, patch=0)
    assert Tag(major=1, minor=0, patch=1) >= Tag(major=1, minor=0, patch=1)
