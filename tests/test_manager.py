import os
from unittest import mock

from application.manager import ReleaseManager
from application.tag import Tag


def test_parse_proper_changelog():
    manager = ReleaseManager(
        project_id=1,
        gitlab_access_token="1",
        changelog_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "fixtures/proper_changelog.txt")
    )

    manager.parse_changelog()

    expected_tag = Tag(major=2)
    expected_notes = [
        "### Added",
        "- Something",
        "### Changed",
        "- Something else",
        "### Fixed",
        "- Yet another different thing"
    ]

    assert manager.tag == expected_tag
    assert manager.notes == expected_notes


def test_parse_changelog_without_tag():
    manager = ReleaseManager(
        project_id=1,
        gitlab_access_token="1",
        changelog_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "fixtures/changelog_without_tag.txt")
    )

    manager.parse_changelog()

    assert manager.tag is None
    assert manager.notes == []


def test_parse_changelog_without_notes():
    manager = ReleaseManager(
        project_id=1,
        gitlab_access_token="1",
        changelog_path=os.path.join(os.path.dirname(os.path.abspath(__file__)), "fixtures/changelog_without_notes.txt")
    )

    manager.parse_changelog()

    assert manager.tag == Tag(major=2)
    assert manager.notes == []


@mock.patch("application.manager.ReleaseManager.parse_changelog")
@mock.patch("application.manager.ReleaseManager.get_latest_project_tag")
@mock.patch("application.manager.ReleaseManager.message_no_new_release_specified")
def test_run_no_new_release(
    mocked_message_no_new_release_specified,
    mocked_get_latest_project_tag,
    mocked_parse_changelog
):
    mocked_get_latest_project_tag.return_value = Tag(major=2)
    manager = ReleaseManager(project_id=1, gitlab_access_token="1")
    manager.tag = Tag(major=1)

    manager.run()

    assert mocked_message_no_new_release_specified.called


@mock.patch("application.manager.ReleaseManager.parse_changelog")
@mock.patch("application.manager.ReleaseManager.get_latest_project_tag")
@mock.patch("application.manager.ReleaseManager.message_creating_new_release")
@mock.patch("application.manager.ReleaseManager.create_new_tag")
def test_run_new_release(
    mocked_create_new_tag,
    mocked_message_creating_new_release,
    mocked_get_latest_project_tag,
    mocked_parse_changelog
):
    mocked_get_latest_project_tag.return_value = Tag(major=1)
    manager = ReleaseManager(project_id=1, gitlab_access_token="1")
    manager.tag = Tag(major=2)

    manager.run()

    assert mocked_message_creating_new_release.called
    assert mocked_create_new_tag.called
