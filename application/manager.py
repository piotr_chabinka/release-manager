from application.gitlab_api import GitlabAPI
from application.tag import Tag


class ReleaseManager:

    def __init__(self, project_id, gitlab_access_token, changelog_path="CHANGELOG.md"):
        self.project_id = project_id
        self.gitlab_api = GitlabAPI(access_token=gitlab_access_token)
        self.tag = None
        self.notes = []
        self.changelog_path = changelog_path

    @staticmethod
    def build_tag_versions(tag_name):
        versions = tag_name.split(".")
        tag_versions = {
            "major": versions[0]
        }
        try:
            tag_versions["minor"] = versions[1]
        except IndexError:
            pass

        try:
            tag_versions["patch"] = versions[2]
        except IndexError:
            pass

        return tag_versions

    def parse_changelog(self):
        with open(self.changelog_path, "r") as changelog_file:
            for line in filter(lambda x: x.strip() != "", changelog_file.readlines()):
                if line.startswith("## "):
                    if self.tag is None:
                        self.tag = Tag(**self.build_tag_versions(tag_name=line[line.find("[") + 1:line.find("]")]))
                    else:
                        break
                else:
                    if self.tag is not None:
                        self.notes.append(line.replace("\n", ""))

    def get_latest_project_tag(self):
        return self.gitlab_api.get_tags(project_id=self.project_id)[0]["name"]

    def create_new_tag(self):
        return self.gitlab_api.create_tag(
            project_id=self.project_id,
            tag_name=self.tag,
            tag_release_notes=self.notes
        )

    def message_got_latest_release(self, latest_tag):
        print(f"Got latest release for project {self.project_id}: {latest_tag}")

    @staticmethod
    def message_no_new_release_specified():
        print(f"It seems that there is no new release specified in CHANGELOG.md, skipping")

    def message_creating_new_release(self):
        print(f"Creating new release {self.tag} with release notes list: {self.notes}")

    def run(self):
        self.parse_changelog()
        latest_tag = self.get_latest_project_tag()
        self.message_got_latest_release(latest_tag)
        if latest_tag >= self.tag:
            self.message_no_new_release_specified()
        else:
            self.message_creating_new_release()
            self.create_new_tag()
