from requests import request


class GitlabAPI:

    GITLAB_PROJECTS_URL = 'https://gitlab.com/api/v4/projects'

    def __init__(self, access_token):
        self.access_token = access_token

    def send_request(self, url, data=None, files=None, headers=None, method="GET"):
        if headers is None:
            request_headers = {
                "PRIVATE-TOKEN": self.access_token
            }
        else:
            request_headers = {
                **{
                    "PRIVATE-TOKEN": self.access_token
                },
                **headers
            }

        response = request(
            url=url,
            data=data,
            files=files,
            headers=request_headers,
            method=method
        )
        response.raise_for_status()
        return response

    def get_tags(self, project_id):
        return self.send_request(
            url=f"{self.GITLAB_PROJECTS_URL}/{project_id}/repository/tags",
        )

    @staticmethod
    def build_release_description(tag_release_notes):
        return "\n".join(tag_release_notes)

    def build_tag_url_parameters(self, tag_name, tag_release_notes):
        return f"tag_name={tag_name}&release_description={self.build_release_description(tag_release_notes)}"

    def create_tag(self, project_id, tag_name, tag_release_notes):
        url_parameters = self.build_tag_url_parameters(tag_name=tag_name, tag_release_notes=tag_release_notes)
        return self.send_request(
            url=f"{self.GITLAB_PROJECTS_URL}/{project_id}/repository/tags?{url_parameters}",
            method="POST"
        )
