class Tag:

    def __init__(self, major, minor=0, patch=0):
        self.major = major
        self.minor = minor
        self.patch = patch
        self.value = 100 * int(major) + 10 * int(minor) + int(patch)

    def __repr__(self):
        return f"{self.major}.{self.minor}.{self.patch}"

    def __eq__(self, other):
        return self.value == other.value

    def __lt__(self, other):
        return self.value < other.value

    def __le__(self, other):
        return self.__lt__(other) or self.__eq__(other)

    def __gt__(self, other):
        return not self == other and not self < other

    def __ge__(self, other):
        return self.__gt__(other) or self.__eq__(other)
